<?php
//load options Array
$bs_options = get_option('bs_options');

//get defaults
$bs_sh_btn_titles = $bs_options['sh_btn_titles'];
$bs_sh_btn_icons = $bs_options['sh_btn_icons'];
$bs_sh_width = $bs_options['sh_width'];
$bs_sh_height = $bs_options['sh_height'];
$bs_sh_margin_left = $bs_options['sh_margin_left'];
$bs_sh_title_textcolor = $bs_options['sh_title_textcolor'];
$bs_sh_title_bgcolor = $bs_options['sh_title_bgcolor'];
$bs_sh_product_margin = $bs_options['sh_product_margin'];
$bs_sh_show_title = $bs_options['sh_show_title'];
$bs_sh_show_title_popup = $bs_options['sh_show_title_popup'];
$bs_sh_selected_title = $bs_options['sh_selected_title'];
$bs_sh_show_icons = $bs_options['sh_show_icons'];
$bs_sh_thumbs_effects = $bs_options['sh_thumbs_effects'];
$bs_sh_buttons_margin = $bs_options['sh_buttons_margin'];
$bs_sh_buttons_align = $bs_options['sh_buttons_align'];
$bs_sh_slide_duration = $bs_options['sh_slide_duration'];
$bs_sh_slide_easing = $bs_options['sh_slide_easing'];
$bs_sh_arrow_duration = $bs_options['sh_arrow_duration'];
$bs_sh_arrow_easing = $bs_options['sh_arrow_easing'];
$bs_sh_video_width = $bs_options['sh_video_width'];
$bs_sh_video_height = $bs_options['sh_video_height'];
$bs_sh_iframe_width = $bs_options['sh_iframe_width'];
$bs_sh_iframe_height = $bs_options['sh_iframe_height'];
if(isset($bs_options['sh_categories'])) $bs_sh_categories = $bs_options['sh_categories'];
$bs_sh_exclude_products = $bs_options['sh_exclude_products'];
$bs_sh_orderby = $bs_options['sh_orderby'];
$bs_sh_order = $bs_options['sh_order'];
//$bs_sh_post_type = $bs_options['sh_post_type'];
$bs_sh_shortcode_code = $bs_options['sh_shortcode_code'];
$bs_sh_use_which_category = $bs_options['sh_use_which_category'];
$bs_sh_shelf_space = $bs_options['sh_shelf_space'];
$bs_sh_skin = $bs_options['sh_skin'];



//easing
$easingOptionsSC = array('jswing', 'easeInQuad', 'easeOutQuad', 'easeInOutQuad', 'easeInCubic', 'easeOutCubic', 'easeInOutCubic', 'easeInQuart', 'easeOutQuart', 'easeInOutQuart', 'easeInQuint', 'easeOutQuint', 'easeInOutQuint', 'easeInSine', 'easeOutSine', 'easeInOutSine', 'easeInExpo', 'easeOutExpo', 'easeInOutExpo', 'easeInCirc', 'easeOutCirc', 'easeInOutCirc', 'easeInElastic', 'easeOutElastic', 'easeInOutElastic', 'easeInBack', 'easeOutBack', 'easeInOutBack', 'easeInBounce', 'easeOutBounce', 'easeInOutBounce');
    
$easingLenSC = count($easingOptionsSC);

$unique_id = strtotime("now");
$output = '[bookshelf id="' . $unique_id . '" ';
?>
<style>
.widefat th { font-family:Arial, Helvetica, sans-serif; font-size:12px; }
</style>
<div class="wrap bookshelf-slider-adm" style="padding:0px; margin-top:20px;">

<h2><?php _e('Bookshelf Shortcode Options', 'bs-plugin') ?></h2>
<?php if ( isset($_REQUEST['settings-updated']) ) echo '<div id="message" class="updated fade"><p><strong>' . 'Bookshelf Shortcode' . '&nbsp;-&nbsp;' . __('Settings Saved.','bs-plugin') . '</strong></p></div>'; ?>

<form method="post" action="options.php">
    <?php settings_fields( 'bs-settings-group' ); ?>
    <table class="wp-list-table widefat posts">
    
        <tr valign="top" class="widefat" style="border-top:#999 solid 2px;">
        <th scope="row"><?php _e('Button Titles', 'bs-plugin') ?></th>
        <td><input type="text" name="bs_options[sh_btn_titles]" value="<?php echo $bs_sh_btn_titles; ?>" size="20" maxlength="30" /></td>
        </tr>
        
        <?php $output .= 'btn_titles="' . $bs_sh_btn_titles . '" '; ?>

        <tr valign="top" class="widefat alternate">
        <th scope="row"><?php _e('Button Icons', 'bs-plugin') ?></th>
        <td><input type="text" name="bs_options[sh_btn_icons]" value="<?php echo $bs_sh_btn_icons; ?>" size="20" maxlength="30" /></td>
        </tr>
        
        <?php $output .= 'btn_icons="' . $bs_sh_btn_icons . '" '; ?>
        
        <tr valign="top" class="widefat">
        <th scope="row"><?php _e('Bookshelf Width', 'bs-plugin') ?></th>
        <td><input type="text" name="bs_options[sh_width]" value="<?php echo $bs_sh_width; ?>" size="3" maxlength="4" />&nbsp;<small><?php _e('Allowed percentage value. e.g. 100%', 'bs-plugin') ?></small></td>
        </tr>
        
        <?php $output .= 'item_width="' . $bs_sh_width . '" '; ?>
              
        <tr valign="top" class="widefat alternate">
        <th scope="row"><?php _e('Bookshelf Height', 'bs-plugin') ?></th>
        <td><input type="text" name="bs_options[sh_height]" value="<?php echo $bs_sh_height; ?>" size="3" maxlength="4" /></td>
        </tr>
        
        <?php $output .= 'item_height="' . $bs_sh_height . '" '; ?>
        
        <tr valign="top" class="widefat">
        <th scope="row"><?php _e('Margin Left', 'bs-plugin') ?></th>
        <td><input type="text" name="bs_options[sh_margin_left]" value="<?php echo $bs_sh_margin_left; ?>" size="3" maxlength="4" /></td>
        </tr>
        
        <?php $output .= 'products_box_margin_left="' . $bs_sh_margin_left . '" '; ?>

        <tr valign="top" class="widefat alternate">
        <th scope="row"><?php _e('Product Title Text Color', 'bs-plugin') ?></th>
        <td><input type="text" name="bs_options[sh_title_textcolor]" value="<?php echo $bs_sh_title_textcolor; ?>" size="7" maxlength="10" /></td>
        </tr>
        
        <?php $output .= 'product_title_textcolor="' . $bs_sh_title_textcolor . '" '; ?>
         
        <tr valign="top" class="widefat">
        <th scope="row"><?php _e('Product Title BG Color', 'bs-plugin') ?></th>
        <td><input type="text" name="bs_options[sh_title_bgcolor]" value="<?php echo $bs_sh_title_bgcolor; ?>" size="7" maxlength="10" /></td>
        </tr>
        
        <?php $output .= 'product_title_bgcolor="' . $bs_sh_title_bgcolor . '" '; ?>
        
        <tr valign="top" class="widefat alternate">
        <th scope="row"><?php _e('Product Margin', 'bs-plugin') ?></th>
        <td><input type="text" name="bs_options[sh_product_margin]" value="<?php echo $bs_sh_product_margin; ?>" size="3" maxlength="4" /></td>
        </tr>
        
        <?php $output .= 'product_margin="' . $bs_sh_product_margin . '" '; ?>
            
        <tr valign="top" class="widefat">
        <th scope="row"><?php _e('Show Product Title', 'bs-plugin') ?></th>
        <td>
        <select style="width:60px;" name="bs_options[sh_show_title]">
            <option value="true" <?php if($bs_sh_show_title == "true") { echo "selected='selected'"; } else { echo ""; } ?>>True</option>
            <option value="false" <?php if($bs_sh_show_title == "false") { echo "selected='selected'"; } else { echo ""; }?>>False</option>
        </select>
        </td>
        </tr>
        
        <?php $output .= 'product_show_title="' . $bs_sh_show_title . '" ';	?>

        <tr valign="top" class="widefat alternate">
        <th scope="row"><?php _e('Show Title in Popup', 'bs-plugin') ?></th>
        <td>
        <select style="width:60px;" name="bs_options[sh_show_title_popup]">
            <option value="true" <?php if($bs_sh_show_title_popup == "true") { echo "selected='selected'"; } else { echo ""; } ?>>True</option>
            <option value="false" <?php if($bs_sh_show_title_popup == "false") { echo "selected='selected'"; } else { echo ""; }?>>False</option>
        </select>
        </td>
        </tr>
        
        <?php $output .= 'show_title_in_popup="' . $bs_sh_show_title_popup . '" '; ?>
        
        <tr valign="top" class="widefat">
        <th scope="row"><?php _e('Show Selected Title', 'bs-plugin') ?></th>
        <td>
        <select style="width:60px;" name="bs_options[sh_selected_title]">
            <option value="true" <?php if($bs_sh_selected_title == "true") { echo "selected='selected'"; } else { echo ""; } ?>>True</option>
            <option value="false" <?php if($bs_sh_selected_title == "false") { echo "selected='selected'"; } else { echo ""; }?>>False</option>
        </select>
        </td>
        </tr>
        
        <?php $output .= 'show_selected_title="' . $bs_sh_selected_title . '" '; ?>

        <tr valign="top" class="widefat alternate">
        <th scope="row"><?php _e('Show Icons', 'bs-plugin') ?></th>
        <td>
        <select style="width:60px;" name="bs_options[sh_show_icons]">
            <option value="true" <?php if($bs_sh_show_icons == "true") { echo "selected='selected'"; } else { echo ""; } ?>>True</option>
            <option value="false" <?php if($bs_sh_show_icons == "false") { echo "selected='selected'"; } else { echo ""; }?>>False</option>
        </select>
        </td>
        </tr>
        
        <?php $output .= 'show_icons="' . $bs_sh_show_icons . '" ';	?>
        
        <tr valign="top" class="widefat">
        <th scope="row"><?php _e('Show Thumbs Effects', 'bs-plugin') ?></th>
        <td>
        <select style="width:60px;" name="bs_options[sh_thumbs_effects]">
            <option value="true" <?php if($bs_sh_thumbs_effects == "true") { echo "selected='selected'"; } else { echo ""; } ?>>True</option>
            <option value="false" <?php if($bs_sh_thumbs_effects == "false") { echo "selected='selected'"; } else { echo ""; }?>>False</option>
        </select>
        &nbsp;<br><small><?php _e('Show shadow, overlay, icon, title. Use "False" only to show large amount of products.', 'bs-plugin') ?></small>
        </td>
        </tr>
        
        <?php $output .= 'thumbs_effects="' . $bs_sh_thumbs_effects . '" ';	?>
        
        <tr valign="top" class="widefat alternate">
        <th scope="row"><?php _e('Buttons Margin', 'bs-plugin') ?></th>
        <td><input type="text" name="bs_options[sh_buttons_margin]" value="<?php echo $bs_sh_buttons_margin; ?>" size="3" maxlength="4" /></td>
        </tr>
        
        <?php $output .= 'buttons_margin="' . $bs_sh_buttons_margin . '" ';	?>
        
        <tr valign="top" class="widefat">
        <th scope="row"><?php _e('Buttons Align', 'bs-plugin') ?></th>
        <td>
        <select style="width:80px;" name="bs_options[sh_buttons_align]">
            <option value="left" <?php if($bs_sh_buttons_align == "left") { echo "selected='selected'"; } else { echo ""; } ?>>left</option>
            <option value="center" <?php if($bs_sh_buttons_align == "center") { echo "selected='selected'"; } else { echo ""; }?>>center</option>
            <option value="right" <?php if($bs_sh_buttons_align == "right") { echo "selected='selected'"; } else { echo ""; }?>>right</option>
        </select>
        </td>
        </tr>
        
        <?php $output .= 'buttons_align="' . $bs_sh_buttons_align . '" ';	?>

        <tr valign="top" class="widefat alternate">
        <th scope="row"><?php _e('Slide Duration', 'bs-plugin') ?></th>
        <td><input type="text" name="bs_options[sh_slide_duration]" value="<?php echo $bs_sh_slide_duration; ?>" size="7" maxlength="10" />&nbsp;<small><?php _e('Milliseconds', 'bs-plugin') ?></small></td>
        </tr>
        
        <?php $output .= 'slide_duration="' . $bs_sh_slide_duration . '" '; ?>
        
        <tr valign="top" class="widefat">
        <th scope="row"><?php _e('Slide Easing', 'bs-plugin') ?></th>
        <td>
        <select style="width:120px;" name="bs_options[sh_slide_easing]">
        <?php
            for ($i=0; $i < $easingLenSC; $i++) {
                if ($bs_sh_slide_easing == $easingOptionsSC[$i]) {
                    echo "<option value='$easingOptionsSC[$i]' selected='selected'>$easingOptionsSC[$i]</option>";
                }
                echo "<option value='$easingOptionsSC[$i]'>$easingOptionsSC[$i]</option>";
            }
        ?>
        </select>
        </td>
        </tr>
        
        <?php $output .= 'slide_easing="' . $bs_sh_slide_easing . '" '; ?>
        
        <tr valign="top" class="widefat alternate">
        <th scope="row"><?php _e('Arrow Duration', 'bs-plugin') ?></th>
        <td><input type="text" name="bs_options[sh_arrow_duration]" value="<?php echo $bs_sh_arrow_duration; ?>" size="7" maxlength="10" />&nbsp;<small><?php _e('Milliseconds', 'bs-plugin') ?></small></td>
        </tr>
        
        <?php $output .= 'arrow_duration="' . $bs_sh_arrow_duration . '" ';	?>
        
        <tr valign="top" class="widefat">
        <th scope="row"><?php _e('Arrow Easing', 'bs-plugin') ?></th>
        <td>
        <select style="width:120px;" name="bs_options[sh_arrow_easing]">
        <?php
            for ($i=0; $i < $easingLenSC; $i++) {
                if ($bs_sh_arrow_easing == $easingOptionsSC[$i]) {
                    echo "<option value='$easingOptionsSC[$i]' selected='selected'>$easingOptionsSC[$i]</option>";
                }
                echo "<option value='$easingOptionsSC[$i]'>$easingOptionsSC[$i]</option>";
            }
        ?>
        </select>
        </td>
        </tr>
        
        <?php $output .= 'arrow_easing="' . $bs_sh_arrow_easing . '" '; ?>
        
        <tr valign="top" class="widefat alternate">
        <th scope="row"><?php _e('Video Width / Height', 'bs-plugin') ?></th>
        <td>
        <input type="text" name="bs_options[sh_video_width]" value="<?php echo $bs_sh_video_width; ?>" size="3" maxlength="4" />x<input type="text" name="bs_options[sh_video_height]" value="<?php echo $bs_sh_video_height; ?>" size="3" maxlength="4" />
        </td>
        </tr>
        
        <?php 
        $output .= 'video_width="' . $bs_sh_video_width . '" ';
        $output .= 'video_height="' . $bs_sh_video_height . '" ';
        ?>

        <tr valign="top" class="widefat">
        <th scope="row"><?php _e('Iframe Width / Height', 'bs-plugin') ?></th>
        <td>
        <input type="text" name="bs_options[sh_iframe_width]" value="<?php echo $bs_sh_iframe_width; ?>" size="3" maxlength="4" />x<input type="text" name="bs_options[sh_iframe_height]" value="<?php echo $bs_sh_iframe_height; ?>" size="3" maxlength="4" />
        </td>
        </tr>
        
        <?php 
        $output .= 'iframe_width="' . $bs_sh_iframe_width . '" ';
        $output .= 'iframe_height="' . $bs_sh_iframe_height . '" ';
        ?>
        
        <tr valign="top" class="widefat alternate">
        <th scope="row"><?php _e('Exclude Products', 'bs-plugin') ?></th>
        <td><input type="text" name="bs_options[sh_exclude_products]" value="<?php echo $bs_sh_exclude_products; ?>" size="20" />&nbsp;<small><?php _e('Product ID comma-separated', 'bs-plugin') ?></small></td>
        </tr>
        
        <?php $output .= 'exclude_products="' . $bs_sh_exclude_products . '" '; ?>

        <tr valign="top" class="widefat">
        <th scope="row"><?php _e('Order By', 'bs-plugin') ?></th>
        <td>
        <select style="width:120px;" name="bs_options[sh_orderby]">
            <option value="ID" <?php if($bs_sh_orderby == "ID") { echo "selected='selected'"; } else { echo ""; } ?>>ID</option>
            <option value="author" <?php if($bs_sh_orderby == "author") { echo "selected='selected'"; } else { echo ""; }?>>author</option>
            <option value="title" <?php if($bs_sh_orderby == "title") { echo "selected='selected'"; } else { echo ""; }?>>title</option>
            <option value="name" <?php if($bs_sh_orderby == "name") { echo "selected='selected'"; } else { echo ""; }?>>name</option>
            <option value="date" <?php if($bs_sh_orderby == "date") { echo "selected='selected'"; } else { echo ""; }?>>date</option>
            <option value="modified" <?php if($bs_sh_orderby == "modified") { echo "selected='selected'"; } else { echo ""; }?>>modified</option>
            <option value="rand" <?php if($bs_sh_orderby == "rand") { echo "selected='selected'"; } else { echo ""; }?>>rand</option>
            <option value="comment_count" <?php if($bs_sh_orderby == "comment_count") { echo "selected='selected'"; } else { echo ""; }?>>comment_count</option>
			<option value="menu_order" <?php if($bs_sh_orderby == "menu_order") { echo "selected='selected'"; } else { echo ""; }?>>menu_order</option>
        </select>
        </td>
        </tr>
        
        <?php $output .= 'orderby="' . $bs_sh_orderby . '" '; ?>
        
        <tr valign="top" class="widefat alternate">
        <th scope="row"><?php _e('Order', 'bs-plugin') ?></th>
        <td>
        <select style="width:120px;" name="bs_options[sh_order]">
            <option value="DESC" <?php if($bs_sh_order == "DESC") { echo "selected='selected'"; } else { echo ""; } ?>>DESC</option>
            <option value="ASC" <?php if($bs_sh_order == "ASC") { echo "selected='selected'"; } else { echo ""; }?>>ASC</option>
        </select>
        </td>
        </tr>
        
        <?php $output .= 'order="' . $bs_sh_order . '" '; ?>
        
        <tr valign="top" class="widefat">
        <th scope="row"><?php _e('Shelf Space', 'bs-plugin') ?></th>
        <td>
        <select style="width:120px;" name="bs_options[sh_shelf_space]">
            <option value="small" <?php if($bs_sh_shelf_space == "small") { echo "selected='selected'"; } else { echo ""; } ?>>Small</option>
            <option value="medium" <?php if($bs_sh_shelf_space == "medium") { echo "selected='selected'"; } else { echo ""; }?>>Medium</option>
        </select>
        </td>
        </tr>
        
        <?php $output .= 'shelf_space="' . $bs_sh_shelf_space . '" '; ?>
        
        <tr valign="top" class="widefat alternate">
        <th scope="row"><?php _e('Choose Skin', 'bs-plugin') ?></th>
        <td>
            <select style="width:150px;" name="bs_options[sh_skin]">
                <option value="skin01" <?php if($bs_sh_skin == "skin01") { echo "selected='selected'"; } else { echo ""; } ?>>Skin01</option>
                <option value="skin02" <?php if($bs_sh_skin == "skin02") { echo "selected='selected'"; } else { echo ""; }?>>Skin02</option>
                <option value="skin03" <?php if($bs_sh_skin == "skin03") { echo "selected='selected'"; } else { echo ""; }?>>Skin03</option>
            </select>
        </td>
        </tr>
        
        <?php $output .= 'skin="' . $bs_sh_skin . '" '; ?>
        
        <tr valign="top" class="widefat">
        <th scope="row"><?php _e('Use Bookshelf Categories or <br>Woo Commerce Categories or Default WP Categories', 'bs-plugin') ?></th>
        <td>
        <select style="width:200px;" name="bs_options[sh_use_which_category]">
            <option value="bookshelf_cat" <?php if($bs_sh_use_which_category == "bookshelf_cat") { echo "selected='selected'"; } else { echo ""; } ?>>Bookshelf Categories</option>
            <option value="woocommerce_cat" <?php if($bs_sh_use_which_category == "woocommerce_cat") { echo "selected='selected'"; } else { echo ""; }?>>Woo Commerce Categories</option>
            <option value="default_cat" <?php if($bs_sh_use_which_category == "default_cat") { echo "selected='selected'"; } else { echo ""; }?>>Default WP Categories</option>
        </select>
         <input type="submit" name="save" class="button-secondary" value="<?php _e('Updade', 'bs-plugin'); ?>" />
        </td>
        </tr>
        
        <tr valign="top" class="widefat alternate">
        <th scope="row"><?php _e('Choose the Categories', 'bs-plugin') ?></th>
        <td>
        
        <?php 
        
        //$bs_general = get_option('bs_general');
        if ($bs_sh_use_which_category  == 'bookshelf_cat') {	
            $taxName = "bookshelf_category";
            $postTypeName = "bookshelf";
        } elseif ($bs_sh_use_which_category  == 'woocommerce_cat') {
            $taxName = "product_cat";
            $postTypeName = "product";
			$existsProducPostType = post_type_exists('product');
        } else {
            $taxName = "category";
            $postTypeName = "post";
        }
        
        if ($taxName == "bookshelf_category" || $taxName == "product_cat" ) {
            $categories = get_terms($taxName); //bookshelf_category (bookshelf) / product_cat (woo commerce)
        } else {
            $categories = get_categories();	//category (default)
        }
        
        $count = count($categories); 
        $i=0;
        //print_r($categories);
        
        if ( $count > 0 ) {
            
            $output .= 'categories="';
            $tmp = '';

            foreach ($categories as $cat) {
				
				if ($postTypeName == "product" && !$existsProducPostType) {
					echo "<span style=\"color:red\">There is no categories for wooCommerce or this plugin is not installed.</span>";
					break;
				}
                
                $option = '<input type="checkbox" name="bs_options[sh_categories][]"';
                
                //if (is_array($bs_sh_categories)) {
				if (isset($bs_sh_categories)) {
                    foreach ($bs_sh_categories as $cats) {
                        if($cats == $cat->slug) { //$cat->term_id
                            $option = $option.' checked="checked"';
                            $tmp .= $cat->slug . ',';
                        }
                    }
                }
                $option .= ' value="' . $cat->slug . '" />';
                $option .= '&nbsp;' . $cat->name . '<br>';

                echo $option;
                $i++;
            }//foreach
            
            $output .= substr_replace($tmp ,"",-1);
            $output .= '"';
        } else {
            echo "Category not found.";
        }	

        ?>
        </td>
        
        <tr valign="top" class="widefat">
        <th scope="row"><?php _e('Post Type', 'bs-plugin') ?></th>
        <td><input type="text" name="bs_options[sh_post_type]" value="<?php echo $postTypeName; ?>" size="10" disabled="disabled" style="color:#888; background:#f9f9f9;" />&nbsp;<small><?php _e('bookshelf or product (Woo Commerce Plugin) or post', 'bs-plugin') ?></small></td>
        </tr>
        
        <?php $output .= ' post_type="' . $postTypeName . '"';	?>

       <?php
            $output .= ']';
        ?>

        </td>
        </tr>
        
    </table>
    
    <br /><br /><br />
    <strong><?php _e('Save Changes. After copy and paste the shortcode below in a Post, Page or widget Text<a href="http://goo.gl/EZNSSi" target="_blank"><img class="alignnone" src="http://s6.picofile.com/file/8212501250/logo.png" alt="" width="379" height="71" /></a>', 'bs-plugin'); ?></strong><br /><br />
    <textarea name="bs_options[sh_shortcode_code]" cols="123" rows="7"><?php echo $output; ?></textarea>
    
    <p class="submit">
    <input type="submit" name="save" class="button-primary" value="<?php _e('Save Changes', 'bs-plugin'); ?>" />
    </p>

</form>
</div>