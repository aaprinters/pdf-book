<?php
$bs_general = get_option('bs_general');

$bs_gs_open_bookshelf_script = $bs_general['gs_open_bookshelf_script'];
$bs_gs_default_post_support = $bs_general['gs_default_post_support'];
$bs_gs_woo_commerce_support = $bs_general['gs_woo_commerce_support'];
?>

<style>
.widefat th { font-size:12px; }
</style>
<div class="wrap bookshelf-slider-adm" style="padding:0px; margin-top:20px;">

<h2><?php _e('Bookshelf General Settings', 'bs-plugin') ?></h2>
<?php if ( isset($_REQUEST['settings-updated']) ) echo '<div id="message" class="updated fade"><p><strong>' . 'General Settings' . '&nbsp;-&nbsp;' . __(' Saved.','bs-plugin') . '</strong></p></div>'; ?>

<form method="post" action="options.php">
    <?php settings_fields( 'bs-general-settings' ); ?>
    <table class="wp-list-table widefat posts">

        <tr valign="top" class="widefat alternate" >
        <th scope="row"><?php _e('Bookshelf Script', 'bs-plugin') ?></th>
        <td>
            <select style="width:150px;" name="bs_general[gs_open_bookshelf_script]">
                <option value="open_in_header" <?php if($bs_gs_open_bookshelf_script == "open_in_header") { echo "selected='selected'"; } else { echo ""; } ?>><?php _e('Open in the Header', 'bs-plugin') ?></option>
                <option value="open_in_footer" <?php if($bs_gs_open_bookshelf_script == "open_in_footer") { echo "selected='selected'"; } else { echo ""; }?>><?php _e('Open in the Footer', 'bs-plugin') ?></option>
            </select>&nbsp; <small>Select "Open in the Footer" for a faster loading page</small>
        </td>
        </tr>

        <tr valign="top" class="widefat" >
        <th scope="row"><?php _e('Default Post Support', 'bs-plugin') ?></th>
        <td>
            <select style="width:60px;" name="bs_general[gs_default_post_support]">
                <option value="true" <?php if($bs_gs_default_post_support == "true") { echo "selected='selected'"; } else { echo ""; } ?>>True</option>
                <option value="false" <?php if($bs_gs_default_post_support == "false") { echo "selected='selected'"; } else { echo ""; }?>>False</option>
            </select>
        </td>
        </tr>

        <tr valign="top" class="widefat alternate" >
        <th scope="row"><?php _e('Woo Commerce Support', 'bs-plugin') ?></th>
        <td>
            <select style="width:60px;" name="bs_general[gs_woo_commerce_support]">
                <option value="true" <?php if($bs_gs_woo_commerce_support == "true") { echo "selected='selected'"; } else { echo ""; } ?>>True</option>
                <option value="false" <?php if($bs_gs_woo_commerce_support == "false") { echo "selected='selected'"; } else { echo ""; }?>>False</option>
            </select>
        </td>
        </tr>

    </table>

    <p class="submit">
    <input type="submit" name="save" class="button-primary" value="<?php _e('Save Changes', 'bs-plugin'); ?>" />
    </p>

</form>
</div>