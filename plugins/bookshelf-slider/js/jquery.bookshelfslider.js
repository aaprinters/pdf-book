/*
 * Bookshelf Slider jQuery Plugin - v2.8
 * Author: Sergio Valle
 * Date: Nov 19, 2013
 * Adapted for Wordpress
 * 
 * If you have questions, suggestions or any issue in the code, please email me through
 * my codecanyon user profile >> http://codecanyon.net/user/srvalle
*/

(function($){

    $.bookshelfSlider = function(selector, settings){

		//parameters
		if ( settings ){$.extend($.bookshelfSlider.config, settings);}

		var $bsc = $.bookshelfSlider.config;

		// ----------------------------------------------
		// declare variables
		// ----------------------------------------------

		var $panel_slider = $(selector + " .panel_slider");
		var $arrow_menu = $(selector + ' #arrow_menu');
		var $slide_animate = $(selector + " .slide_animate");
		var $button_bar = $(selector + " .button_bar");
		var $buttons_container = $(selector + " .buttons_container"); //buttons_align

		var $plugin_dir = $(selector + " .plugin_dir").text();

		if (navigator.userAgent.match(/msie/i) && navigator.userAgent.match(/7/)) { var is_ie7 = true; }
		if (navigator.userAgent.match(/msie/i) && navigator.userAgent.match(/8/)) { var is_ie8 = true; }

		// get value from properties (that will be used after) and set new variables. properties are overlapped if there are more than one instance
		var slide_duration = $bsc.slide_duration;
		var slide_easing = $bsc.slide_easing;
		var arrow_duration = $bsc.arrow_duration;
		var arrow_easing = $bsc.arrow_easing;
		var buttons_margin = $bsc.buttons_margin;

		var show_title_in_popup = $bsc.show_title_in_popup;

		var video_width_height = $bsc.video_width_height;
		var iframe_width_height = $bsc.iframe_width_height;

		////////////////
		var shelf_space = $bsc.shelf_space;
		var css_class = $bsc.css_class;
		var forced_centralized = $bsc.forced_centralized;


		// ----------------------------------------------
		// set interface width/height and positions
		// ----------------------------------------------
		
		//panel and panel bar size, panel title, button size
		$panel_slider.width($bsc.item_width);
		$panel_slider.height($bsc.item_height);	
		$slide_animate.width($panel_slider.width());
		$slide_animate.height($bsc.item_height + 10000);
		
		$button_bar.css('margin-left', buttons_margin);
		
		function alignButtonsCat() {
			if (!is_ie7) {
				switch ($bsc.buttons_align)	{
					case 'left':
						$buttons_container.css('float', 'left');
						break;
					
					case 'center':
						var widthButtons = 0;
						$(selector + ' .button_bar').each(function() {
							widthButtons += $(this).width() + buttons_margin;
						});
						widthButtons += buttons_margin; //20
						var percent = parseInt((widthButtons / $panel_slider.width()) * 100);
						$buttons_container.css('width', percent + '%');
						break;
					
					case 'right':
						$buttons_container.css('float', 'right');
						$buttons_container.css('margin-right', buttons_margin);
						break;
				}
			}
		}
		alignButtonsCat();
		
		$(selector + " .panel_bar").width($bsc.item_width);
		$(selector + " .panel_title").width($bsc.item_width);
		$(selector + " .bs_product").css('margin-right', $bsc.product_margin);
		
		//arrow initial position
		var marginLeft = parseInt($button_bar.css("margin-left"));
		
		var arrow_menu_pos = $(selector + " #btn1").position().left + marginLeft + ($(selector + " #btn1").width() / 2) - 10;
		$arrow_menu.css('left', arrow_menu_pos );
		$arrow_menu.show();
		
		$(selector + ' .panel_items').css('width', '99999');
		
		//selected title
		if ($bsc.show_selected_title)	{ 
			$(selector + ' .selected_title_box').show();
			$(selector + ' .selected_title').text( $(selector + ' #btn1').text() );
		}
		
		//////////////////////////////
		if (!forced_centralized) {
		
			$(selector + ' .products_box').css('margin-left', $bsc.products_box_margin_left );
		
		} else {
			
			forceCenter();
			
		}//forced_centralized
		
		function forceCenter() {
			
			var $prod_box = $(selector + ' .products_box');
			
			$prod_box.each(function(i) {
				
				var $pbox = $(this);
				var $pboxChildrens = $pbox.find('.bs_product').length;
				var $pboxChildrensWidth = 0;
				
				if ($pboxChildrens > 1) {
					
					for (var k=0; k < $pboxChildrens; k++) {
						
						var pboxChild = $pbox.find('.bs_product').eq(k);
						
						// remove margin right if is last child
						if (k == $pboxChildrens-1) {
							pboxChild.css( { 'margin-right': 0 } );
						}
						
						var pboxChildMargin = parseInt(pboxChild.css('margin-right'));
						$pboxChildrensWidth += pboxChild.width() + pboxChildMargin;
					}

				} else {
					
					var pboxChild = $pbox.find('.bs_product').eq(0);
					pboxChild.css({ 'margin-right': 0 });
					$pboxChildrensWidth = pboxChild.width();				
				}
				
				if ($pboxChildrensWidth > $(selector + ' .panel_slider').width() ) {
					$pboxChildrensWidth = $(selector + ' .panel_slider').width();
					
					if ( $(window).width() < 965 && $(window).width() >= 768 ) {
						// display 3 products
						$pboxChildrensWidth = 420;
					} else if ( $(window).width() < 768 && $(window).width() >= 490 ) {
						// display 2 products
						$pboxChildrensWidth = 280;
					} else if ( $(window).width() < 490 ) {
						// display 1 product
						$pboxChildrensWidth = 100;
					}
				}

				$pbox.css({ 'margin': '0 auto', width: $pboxChildrensWidth });
				
				//console.log ( selector + ' > ' + $pboxChildrensWidth )
				//console.log ( selector + ' > ' + $pboxChildrens )
				//console.log ( selector + ' > ' + $pbox.width() )

			});//each
		}
		
		
		////////////////////////////////////
		// add navigation menu
		
		//.buttons_container, .panel_bar
		var $panel_bar = $(selector + " .panel_bar");
		var $button_items = $(selector + " .button_items");
		var $arrow_box = $(selector + " #arrow_box");
		
		function addMenuNavigation() {
			
			if ($buttons_container.width() > $panel_bar.width()) {
				
				$arrow_box.css({ opacity: 0 });
				//$panel_bar.append("<input type='button' class='nav_minus' /><input type='button' class='nav_more' />");
				$panel_bar.append("<div class='nav_minus'></div><div class='nav_more'></div>");
				var navMinus = $(selector + ' .nav_minus');
				var navMore = $(selector + ' .nav_more');
				navMinus.fadeTo("normal",0);
				navMore.fadeTo("normal",0);
				
				
				navMore.click(function(e) {
					//var lastBtn = $(selector + ' .button_bar').eq( $(selector + ' .button_bar').length - 1).attr('id');
					//if ( $('#' + lastBtn).offset().left + $('#' + lastBtn).width() + 50 < $panel_bar.offset().left + $panel_bar.width() ) return;
					
					if ( $button_items.offset().left + $button_items.width() < $panel_bar.offset().left + $panel_bar.width() ) return;
					$button_items.animate({ left: '+=-50', opacity: 1 }, { duration: 400, easing: arrow_easing });
				});
				
				navMinus.click(function(e) {
					//var firstBtn = $(selector + ' .button_bar').eq(0).attr('id');
					//if ( $('#' + firstBtn).offset().left - $('#' + firstBtn).width() + 50 > $panel_bar.offset().left ) return;
					
					if ( $button_items.offset().left > $panel_bar.offset().left ) return;
					$button_items.animate({ left: '+=+50', opacity: 1 }, { duration: 400, easing: arrow_easing });
				});
				
				
				//mouseover
				$panel_bar.mouseenter(function() { 
					//$more_minus_box.show();
					navMinus.fadeTo("normal",1);
					navMore.fadeTo("normal",1); 
				
				}).mouseleave(function() { 
					navMinus.fadeTo("normal",0);
					navMore.fadeTo("normal",0); 
				});
			}
		
		}//addMenuNavigation
		
		//addMenuNavigation();
		
		/*
		function removeMenuNavigation() {
			$arrow_box.css({ opacity: 1 });
			$(selector + ' .nav_minus').remove();
			$(selector + ' .nav_more').remove();
			alignButtonsCat();
		}
		*/
		
		////////////////////////////////////
		
		
		
		//--------------------------------------
		//each product
		//--------------------------------------
		
		var $bs_product = $(selector + ' .bs_product');
		
		$bs_product.each(function(i) {
			
			var $prod = $(this);			
			
			var has_title = ($prod.attr("title") != "" && $prod.attr("title") != "undefined") ? true : false;
			var img = $prod.children('img');
			var imgHeight = $prod.children('img').attr('height');
			var imgWidth = $prod.children('img').attr('width');
			
			//add class
			$prod.children('img').addClass("img_thumb");

			
			if($bsc.thumbs_effects) {
			
				//add title
				if (has_title && $bsc.product_show_title) {
					$prod.append("<div class='bs_product_title " + css_class + "' id='title_"+i+"'>"+$prod.attr('title')+"</div>");
					var the_title = $(selector + " #title_"+i);
					var the_title_top = imgHeight - ( $(the_title).height() / 2 );
					the_title.css({ left: ( $prod.width() - the_title.width() - 9 ) / 2, top: the_title_top });
	
					//product title > textcolor, bgcolor				
					the_title.css({ color: $bsc.product_title_textcolor, background: $bsc.product_title_bgcolor + ' url(' + $plugin_dir + 'assets/title_product_bg.png) repeat' });
				}
				
				//mouse over
				$prod.mouseenter(function() {
					//title
					$(the_title).css({ overflow: 'visible', 'max-height': 150 });
					
					//icons
					$(selector + ' #i_zoom_' + i).css({'background-position': '-24px 0'});
					$(selector + ' #i_play_' + i).css({'background-position': '-72px 0'});
					$(selector + ' #i_link_' + i).css({'background-position': '-120px 0'});
					$(selector + ' #i_iframe_' + i).css({'background-position': '-168px 0'});
	
				}).mouseleave(function() {
	
					//title
					$(the_title).css({ overflow: 'hidden', 'max-height': 11 });
					
					//icons
					$(selector + ' #i_zoom_' + i).css({'background-position': '0 0'});
					$(selector + ' #i_play_' + i).css({'background-position': '-48px 0'});
					$(selector + ' #i_link_' + i).css({'background-position': '-96px 0'});
					$(selector + ' #i_iframe_' + i).css({'background-position': '-144px 0'});
				});
				
				//add effects
				var data_type = $prod.attr('data-type');
			
				if (data_type == 'book') {
					$prod.append("<img class='fx_book' id='b_" + i + "' + src='" + $plugin_dir + "assets/book_fx.png' />");
					$(selector + ' #b_' + i).css({'height': imgHeight, 'top': $prod.height() - imgHeight});
			
				} else if (data_type == 'magazine') {
					$prod.append("<img class='fx_magazine' id='m_" + i + "' + src='" + $plugin_dir + "assets/magazine_fx.png' />");
					$(selector + ' #m_' + i).css({'height': imgHeight, 'top': $prod.height() - imgHeight});
	
				} else if (data_type == 'cd') {
					$prod.append("<img class='fx_cd' id='cd_" + i + "' + src='" + $plugin_dir + "assets/cd_fx.png' />");
					$(selector + ' #cd_' + i).css({'height': imgHeight, 'top': $prod.height() - imgHeight});
	
				} else if (data_type == 'dvd') {
					$prod.append("<img class='fx_cd' id='dvd_" + i + "' + src='" + $plugin_dir + "assets/dvd_fx.png' />");
					$(selector + ' #dvd_' + i).css({'height': imgHeight, 'top': $prod.height() - imgHeight});
	
				}
				
				//add shadow
				$prod.append("<img class='fx_shadow' id='shd_" + i + "' + src='" + $plugin_dir + "assets/shadow_fx.png' />");
				
				//add icons
				
				if ($bsc.show_icons) {
					var data_url = $prod.attr('data-url');
					var data_popup = $prod.attr('data-popup');
		
					if ( data_url != undefined ) {
					
						if ( data_popup == 'true' && (data_url.indexOf('.jpg') != -1 || data_url.indexOf('.gif') != -1 || data_url.indexOf('.png') != -1) ) {
							$prod.append("<div class='icons_sprite' id='i_zoom_" + i + "'></div>");
							
							var $i_zoom = $(selector + ' #i_zoom_' + i);
							$i_zoom.css({'background-position': '0 0', 
										 'left': (img.width() - $i_zoom.width()) / 2,
										 'top': $prod.height() - imgHeight / 2 - ($i_zoom.height() / 2),
										 'opacity': 1});
								
						} else if ( data_popup == 'true' && (data_url.indexOf('youtube.com') != -1 || data_url.indexOf('vimeo.com') != -1) ) {
							$prod.append("<div class='icons_sprite' id='i_play_" + i + "'></div>");
							
							var $i_play = $(selector + ' #i_play_' + i);
							$i_play.css({'background-position': '-48px 0',
										 'left': (img.width() - $i_play.width()) / 2,
										 'top': $prod.height() - imgHeight / 2 - ($i_play.height() / 2),
										 'opacity': 1});
			
						} else if ( data_popup == 'true' && (data_url != '' && data_url != undefined) ) {
							$prod.append("<div class='icons_sprite' id='i_iframe_" + i + "'></div>");
							
							var $i_iframe = $(selector + ' #i_iframe_' + i);
							$i_iframe.css({'background-position': '-144px 0',
										   'left': (img.width() - $i_iframe.width()) / 2,
										   'top': $prod.height() - imgHeight / 2 - ($i_iframe.height() / 2),
										   'opacity': 1});
							
						} else if ( data_url != '' && data_url != undefined ) {
							$prod.append("<div class='icons_sprite' id='i_link_" + i + "'></div>");
							
							var $i_link = $(selector + ' #i_link_' + i);
							$i_link.css({'background-position': '-96px 0',
										 'left': (img.width() - $i_link.width()) / 2,
										 'top': $prod.height() - imgHeight / 2 - ($i_link.height() / 2),
										 'opacity': 1});
						}
					
					}//if data_url != undefined
				}//add icons
			
			}//$bsc.thumbs_effects
			
			//positions	
			img.css({'width': imgWidth, 'height': imgHeight});
			////img.css('cssText', 'height:' +  imgHeight + 'px !important');
				
			if ( $prod.height() != imgHeight ) {
				var diff = $prod.height() - imgHeight;
				img.css('top', diff);
					
				if($bsc.thumbs_effects) {
					//shadow position
					$(selector + ' #shd_' + i).css({'height': imgHeight, 'left': img.width(), 'top': diff});
					
					if (has_title && $bsc.product_show_title) {
						$(the_title).css('top', ($prod.height() - $(the_title).height() / 2) );
					}
				}
			}
		
		});//each product
		
		
		// -------------------------------
		// click menu buttons - bottom
		// -------------------------------
		
		var left_pos = 0;
		var current_position_x = 0;
		
		
		$button_bar.click(function(e) {
			
			e.preventDefault();
			
			var $btn = $(this);
			
			$button_bar.css({ opacity: 1}); 
			$btn.css({ opacity: .6});
			
			//$btn.addClass('button_bar_active');

			//stop if the button has been clicked
			if (global_page-1 == $btn.index()) return false;
			
			if ( parseInt($slide_animate.css('top')) != 0 ) {
				$slide_animate.animate(
					{ top: 0, opacity: 1 }, { 
						duration: 300
					});
			}
			
			var btn_id = $btn.attr('id');
			var btn_current = $(selector + " #" + btn_id);
			var btn_position = btn_current.position();
			var btn_width = btn_current.width();
	
			//arrow position / animate
			var x = btn_position.left + marginLeft + (btn_width / 2) - 10;
			$arrow_menu
				.animate(
					{ left: x }, {
						duration: arrow_duration,
						easing: arrow_easing,
						complete: function() { // callback
							
							//selected title
							if ($bsc.show_selected_title) $(selector + ' .selected_title').text( btn_current.text() );
						}
					});
			
			//set item position x
			current_position_x = parseInt($(selector + " .panel_slider").css('width')) * $btn.index();	
			
			//slide positions
			left_pos = -current_position_x;
	
			// items animate
			$slide_animate
				.animate(
					{ left: left_pos, top: 0 }, { //px
						duration: slide_duration,
						easing: slide_easing,
						complete: function() {}
					});
						

			//navigation more minus
			page_next_prev = 0;
			$icon_more.css('opacity', .3);
			$icon_minus.css('opacity', .3);

			global_page = $btn.index() + 1;
			
			//add in array vertical navigation options
			if(more_page_arr[0] == undefined) {
				$($slide_animate).each(function(j) {
					more_page_arr[j] = navigationMore();
					global_page++;
				});
				global_page = $btn.index() + 1;
			}
			
			if ( more_page_arr[$btn.index()] ) {
				$icon_more.css('opacity', 1);
				$more_minus_box.show();
				$more_minus_box.fadeTo("normal",1);
				$(selector + ' .panel_slider').mouseenter(function() { $more_minus_box.show(); $more_minus_box.fadeTo("normal",1); }).mouseleave(function() { $more_minus_box.hide(); });
			} else {
				$more_minus_box.hide();	
			}

		});//click menu buttons - bottom
		
	
		// --------------------------------------------
		// more minus navigation
		// --------------------------------------------
		
		var shelf_space_num = 0;
		if (shelf_space == 'small') {
			shelf_space_num = 143;
		} else if (shelf_space == 'medium') {
			shelf_space_num = 209;
		} else if (shelf_space == 'large') {
			shelf_space_num = 253;
		}
		
		var global_page = 0;
		var page_next_prev = 0;
		var more_page_arr = [];

		$(selector + ' .panel_slider').append("<div id='more_minus_box'><input type='button' id='icon_minus' /><input type='button' id='icon_more' /></div>");
		var $more_minus_box = $(selector + ' #more_minus_box');
		var $icon_more = $(selector + ' #icon_more');
		var $icon_minus = $(selector + ' #icon_minus');
		
		function navigationMore() {
			
			//if exist last-child > fix empty categories
			if ($(selector + ' #products_box_' + global_page + ' .bs_product:last-child').length) {
				var last_child = $(selector + ' #products_box_' + global_page + ' .bs_product:last-child');
				var last_child_pos = last_child.height() + last_child.offset().top;
				var panel_slider_pos = $panel_slider.height() + $panel_slider.offset().top;
		
				if (last_child_pos - shelf_space_num < panel_slider_pos) { $icon_more.css('opacity', .3); }
				if (last_child_pos > panel_slider_pos) { return true; } else { return false; }
			}
		}
		
		function navigationMinus() {
			var first_child = $(selector + ' #products_box_' + global_page + ' .bs_product:first-child');
			var first_child_pos = first_child.offset().top;
			var initial_position = $panel_slider.offset().top;
			
			if (first_child_pos + shelf_space_num > initial_position) { $icon_minus.css('opacity', .3); }
			if (first_child_pos < initial_position) { return true; } else { return false; }
		}
		
		$icon_minus.click(function(e) {
			
			if ( $icon_minus.css('opacity') != 1 ) return;
			$(this).attr("disabled", "disabled");

			var minus_page = navigationMinus();
			if (!minus_page) {
				return;
			}
			
			page_next_prev -= shelf_space_num;
			$icon_more.css('opacity', 1);
			
			// items animate
			$slide_animate
				.animate(
					{ left: left_pos, top: -page_next_prev }, {
						duration: slide_duration/2,
						easing: slide_easing,
						complete: function() { 
							$icon_minus.removeAttr("disabled");
						}
					});
			
		});//#icon_minus click
		
		$icon_more.click(function(e) {
			
			if ( $icon_more.css('opacity') != 1 ) return;
			
			$(this).attr("disabled", "disabled");
			
			var more_page = navigationMore();
			if (!more_page) {
				return;
			}
			
			page_next_prev += shelf_space_num;
			$icon_minus.css('opacity', 1);
			
			// items animate
			$slide_animate
				.animate(
					{ left: left_pos, top: -page_next_prev }, {
						duration: slide_duration/2,
						easing: slide_easing,
						complete: function() { 
							$icon_more.removeAttr("disabled"); 
						}
					});
			
		});//#icon_more click


		// -------------------------------
		// click menu top
		// -------------------------------
		
		$(selector + ' .show_hide_titles').click(function(e) {
			e.preventDefault();
			if ( $(selector + ' .bs_product_title').css('opacity') == 1 ) {
				$(selector + ' .bs_product_title').stop().animate({'opacity': 0}, 400);
				$(this).css({'opacity': .5});
			} else {
				$(selector + ' .bs_product_title').stop().animate({'opacity': 1}, 400);
				$(this).css({'opacity': 1});
			}
		});
		
		$(selector + ' .show_hide_icons').click(function(e) {
			e.preventDefault();
			if ( $(selector + ' .icons_sprite').css('opacity') == 1 ) {
				$(selector + ' .icons_sprite').stop().animate({'opacity': 0}, 400);
				$(this).css({'opacity': .5});
			} else {
				$(selector + ' .icons_sprite').stop().animate({'opacity': 1}, 400);
				$(this).css({'opacity': 1});
			}
		});
		
		// -------------------------------
		// click load popup
		// -------------------------------
		
		//var $popupMaxWidth = 0;

		$(selector + ' .bs_product').click(function(e) {
			
			e.preventDefault();
	
			var $p = $(this);
			
			var data_url = $p.attr('data-url');
			var data_popup = $p.attr('data-popup');
			var data_newwindow = $p.attr('data-newwindow');

			if (data_popup != 'true') {
				if (data_newwindow != 'true') {
					//open in self window
					////if (data_url == undefined || data_url == '' ) {	return;	} else { location.href = data_url; return; }
					if (data_url == undefined || data_url == '' ) {	return;	} else { window.open(data_url, '_self'); return; }
				} else {
					//open in new window 
					if (data_url == undefined || data_url == '' ) {	return;	} else { window.open(data_url, '_blank'); return; }
				}
			}
			if ( data_popup == 'true' && (data_url == undefined || data_url == '') ) return;
			
			//add mask_popup
			$('body').append('<div id="mask_popup"></div>');
			$('body').append('<div id="popup_info"></div>');
			$('#popup_info').append("<img id='preload_icon' src='" + $plugin_dir + "assets/preload.gif' />");
			
			var $mask = $('#mask_popup');
			var $popup = $('#popup_info');
		
			//Get width / height
			var winWidth = $(window).width();
			var winHeight = $(window).height();
			var docHeight = $(document).height();

			$mask.css({'width': winWidth,'height': docHeight + winHeight });
			//$mask.fadeIn(400);	
			$mask.fadeTo("normal",0.7);

			//popup
			$popup.css({'top': winHeight / 2 - $popup.height() / 2 , 'left': winWidth / 2 - $popup.width() / 2});
			
			
			var is_img = ( data_url.indexOf('.jpg') != -1 || data_url.indexOf('.jpeg') != -1 || data_url.indexOf('.gif') != -1 || data_url.indexOf('.png') != -1 ) ? true : false;
			var is_youtube = ( data_url.indexOf('youtube.com') != -1 ) ? true : false;
			var is_vimeo = ( data_url.indexOf('vimeo.com') != -1 ) ? true : false;
			var is_content = ( data_url.indexOf('.jpg') == -1 && data_url.indexOf('.gif') == -1 && data_url.indexOf('.png') == -1 && !is_vimeo && !is_youtube ) ? true : false;
			
			if (is_img) {
				var this_item = $p;
				
				if ( is_ie7 || is_ie8 ) {
					var img = $("<img />").attr('src', data_url + "?" + new Date().getTime()).attr('id', 'large_image');
				} else {
					var img = $("<img />").attr('src', data_url).attr('id', 'large_image');
				}	
					
					
				img.load(function() {
					
					var img_width = this.width;
					var img_height = this.height;
					
					//alert( this.height > winHeight );
					
					if (this.width > winWidth || this.height > winHeight) {
						
						////////////////////
						var a = winWidth - 80;
						var b = this.width;
						
						//percentNewWidth = (winWidth/this.width)*100
						var percentA = (a/b) * 100;

						//newHeight = (percentNewWidth/100)*this.height
						this.height = (percentA / 100) * this.height;
						this.width = a;
					}
					
					//$popupMaxWidth = this.width;
					
					popupCenterAnimate(this.width, this.height, img, this_item, 'image');
					$(img).hide();
				});
					  
			} else if (is_youtube) {
				
				if (video_width_height[0] > winWidth) { video_width_height[0] = winWidth - 80; }
				if (video_width_height[1] > winHeight) { video_width_height[1] = winHeight - 80; }
				
				var str_id = data_url.split("?v=");
				var str_params = "?autohide=2&amp;autoplay=1&amp;controls=1&amp;disablekb=0&amp;fs=1&amp;hd=0&amp;loop=0&amp;rel=1&amp;showinfo=0&amp;showsearch=1&amp;wmode=transparent&amp;enablejsapi=1";
				var str_iframe = '<iframe class="video_player" width="' + video_width_height[0] + '" height="' + video_width_height[1] + '" frameborder="0" src="http://www.youtube.com/embed/' + str_id[1] + str_params + '"></iframe>';
				
				//$popupMaxWidth = video_width_height[0];
				
				popupCenterAnimate(video_width_height[0], video_width_height[1], str_iframe, $p);
				
			} else if (is_vimeo) {
					
				if (video_width_height[0] > winWidth) { video_width_height[0] = winWidth - 80; }
				if (video_width_height[1] > winHeight) { video_width_height[1] = winHeight - 80; }

				var str_id = data_url.split("/").pop();
				var str_iframe = '<iframe class="video_player" src="http://player.vimeo.com/video/' + str_id +  '?title=0&amp;byline=0&amp;portrait=0&amp;autoplay=1" width="' + video_width_height[0] + '" height="' + video_width_height[1] + '" frameborder="0"></iframe>';
				
				//$popupMaxWidth = video_width_height[0];
				
				popupCenterAnimate(video_width_height[0], video_width_height[1], str_iframe, $p);

			} else if (is_content) {
				
				var is_content_description;
				
				//data-url="?size=900x400&p=[#description#]"
				if ( data_url.indexOf('?size=') != -1 || data_url.indexOf('[#description#]') != -1 ) {
					if (data_url.indexOf('?size=') != -1) {
						var query = data_url.split("?size=").pop().split("&");
						var size = query[0].split("x");
						var w = parseInt(size[0]), h = parseInt(size[1]);
					} else {
						var w = iframe_width_height[0], h = iframe_width_height[1];
					}
					is_content_description = true;
					
				} else {

					var w = iframe_width_height[0], h = iframe_width_height[1];
					is_content_description = false;
				}
				
				//screen resolution adjust
				if (w > winWidth) { w = winWidth - 80; }
				if (h > winHeight) { h = winHeight - 80; }
			
			
				if (is_content_description == true) {
					// ajax
					var post_id = $p.attr('data-id');
					var post_posttype = $p.attr('data-post-type');

					var data = {
						action: 'bs_query_ajax',
						post_id: post_id,
						post_posttype: post_posttype
					};
					
					///////////
					//console.log ('log: ' + bookshelf_script.ajaxurl)
					
					//$popupMaxWidth = w;
                    //alert(bookshelf_script.ajaxurl);//ok
					
					$.get(bookshelf_script.ajaxurl, data, function(data) {
						var str_div = '<div class="bs_html_content_lightbox">' + data + '</div>';
                        //var str_div = '<iframe class="bs_html_content_lightbox" width="100%" frameborder="0" src="about:blank"></iframe>';
						popupCenterAnimate(w, h, str_div, $p);
					});
					
				} else {
					
					var str_div = '<iframe class="bs_html_content_lightbox" width="100%" frameborder="0" src="' + data_url + '"></iframe>';
					popupCenterAnimate(w, h, str_div, $p);
				}
			}
			

			// ------------------------------------
			// popup center animate / append item
			// ------------------------------------
			
			function popupCenterAnimate(w,h,str,this_elem,content_type) {
				
				////////////////// new
				winWidth = $(window).width();
				winHeight = $(window).height();
				
				//alert( h + 50 > winHeight);
				
				//50 = title space
				if (w + 50 > winWidth || h + 50 > winHeight) {
					
					if (w + 50 > winWidth) {
						w = winWidth - 80;
					}

					if (h + 50 > winHeight) {
						h = winHeight - 80;
					}
				}

				var $popup = $('#popup_info');

				$popup.delay(100).animate({ 'top': winHeight / 2 - $popup.height() / 2,  'left': winWidth / 2 - $popup.width() / 2 }, 400, function() {
					$('#preload_icon').fadeOut(200);
					//$('#preload_icon').remove();
				});

				var posX = (winWidth / 2 - w / 2);
				var posY = (winHeight / 2 - h / 2);

				
				//$popup.delay(100).animate({ 'height': h, 'width': w, 'top': posY,  'left': posX-15 }, 400, function() {
				$popup.delay(100).animate({ 'height': h, 'width': w, 'top': posY,  'left': posX-15, opacity:1 }, 400, function() {
					
					//add img/video
					$popup.append(str);
					$(str).fadeIn(600);
					
					
					//////////////////////new
					//$popupMaxWidth = str.width();
					
					if (content_type == 'image') {	
						w = str.width();
						h = str.height();
						posX = (winWidth / 2 - w / 2);
						posY = (winHeight / 2 - h / 2);
					
						$popup.animate( { 'height': h, 'width': w, 'top': posY,  'left': posX-15 }, { duration: 300 });
					}
					
									
					
					//show title in popup
					if ($(this_elem).attr("title") != "" && $(this_elem).attr("title") != "undefined" && show_title_in_popup) {
						var prod_title = $(this_elem).attr("title");
						$popup.append("<div id='bs_popup_title'><div id='bs_close_button'></div>" + prod_title + "</div>");
						var padd = parseInt($popup.css('padding-left'));
						$("#bs_popup_title").css({ 'width': w + padd, 'top': -(padd*2) - 2 , 'left': 0});
						
						//popup title fade
						$("#bs_popup_title").css( { opacity:0 });
						$("#bs_popup_title").fadeIn(400);
						$("#bs_popup_title").animate( { opacity: 1 }, { duration: 400 });
			
						//btn close
						$('#bs_close_button').click(function () {
							
							$("#mask_popup").animate( { opacity: 0 }, { duration: 400 });
							$("#popup_info").delay(100).animate({ opacity: 0 }, 400, function() {
								$('#mask_popup').hide();
								$('#mask_popup').remove();
								$('#popup_info').remove();
							});

						});
					}

				});

			}
			
	
			// -------------------------------
			// click mask_popup
			// -------------------------------
			
			$('#mask_popup').click(function () {
				$("#mask_popup").animate( { opacity: 0 }, { duration: 400 });
				$("#popup_info").delay(100).animate({ opacity: 0 }, 400, function() {
					$('#mask_popup').hide();
					$('#mask_popup').remove();
					$('#popup_info').remove();
				});
			});
		
		});

		$(selector + ' #btn1').trigger('click');
		setTimeout( function() { $(selector).trigger('resize'); }, 1000);
		
		
		// -------------------------------
		// autoplay slide
		// -------------------------------
		
		/*
		function autoSlide() {
		
			var btnLength = $(selector + ' .button_bar').length;
			var count = 2, btnCurrent, interval;
		
			function nextSlide() {
		
				btnCurrent = $(selector + ' #btn' + count);
				btnCurrent.trigger('click');
		
				if (count >= btnLength) {
					count = 1;
				} else {
					count++;
				}
			}
		
			interval = setInterval(nextSlide, 5000); // move each 5 seconds
		}
		autoSlide();
		*/
		
		
		// -------------------------------
		// Resize functions
		// -------------------------------
		
		function resizeEnd() {
			if (new Date() - rtime < delta) {
				setTimeout(resizeEnd, delta);
			} else {
				timeout = false;
				
				//console.log( ' .panel_slider > ' + $(selector + ' .panel_slider').width() );
				$(selector + " .slide_animate").width( $(selector + ' .panel_slider').width() );
				
				//redefined positions and values.
				global_page = 0;
				more_page_arr[0] = undefined;
				$(selector + " .slide_animate").css('top', 0);
				$(selector + ' #btn1').trigger('click');
				
				//console.log( 'click btn1' )
			}
			
			/////////////////////////////
			$buttons_container.width( $panel_bar.width() );
			
			var widthButtons = 0;
			$(selector + ' .button_bar').each(function() {
				widthButtons += $(this).width() + buttons_margin;
			});
			widthButtons += buttons_margin;
			var percent = parseInt((widthButtons / $panel_bar.width()) * 100);
			$buttons_container.css('width', percent + '%');
						
			if ( $(selector + ' .nav_minus').length < 1 && $buttons_container.width() > $panel_bar.width() ) {
				addMenuNavigation();
			}
			
			/*
			if ( $(selector + ' .nav_minus').length >= 1 && $buttons_container.width() < $panel_bar.width() ) {
				removeMenuNavigation();
				alignButtonsCat();
			}
			*/
		}
		
		var rtime = new Date(1, 1, 2000, 12,00,00);
		var timeout = false;
		var delta = 200;
	
		$(window).resize(function() {
			
			rtime = new Date();
			if (timeout === false) {
				timeout = true;
				setTimeout(resizeEnd, delta);
			}
			
			if ( $(selector).width() > $(window).width() ) {
				$(selector).width( $(window).width() );
				$(selector + " .slide_animate").width( $(selector + ' .panel_slider').width() );
				
				var current_left_pos = $(selector + ' .panel_slider').width() * (global_page - 1);
				$(selector + " .slide_animate").css( 'left', -current_left_pos);
			}
			
			if ( $(selector).width() + 20 <= $(window).width() ) {		
				$(selector).width( '100%' );
				
				//console.log( ' .panel_slider > ' + $(selector + ' .panel_slider').width() );		
				//$(selector + " .slide_animate").width( $(selector + ' .panel_slider').width() );
				
				var current_left_pos = $(selector + ' .panel_slider').width() * (global_page - 1);
				$(selector + " .slide_animate").css( 'left', -current_left_pos);
								
			}
			
			/////////////////////////////
			if (forced_centralized) {
				forceCenter();
			}

			
			//popup resize
			
			var $popup = $('#popup_info');
		  	var $popupTitle = $('#bs_popup_title');
			var winWidth = $(window).width();
			var winHeight = $(window).height();
			var docHeight = $(document).height();
				
            $popup.css({'top': winHeight / 2 - $popup.height() / 2 , 'left': winWidth / 2 - $popup.width() / 2 });
		  
		  	var $mask = $('#mask_popup');
			$mask.css({ 'width': winWidth,'height': docHeight + winHeight });
		  
		  	if ( $popup.width() > winWidth ) {
				$popup.width( winWidth - 140 );
			  	$popupTitle.width( $popup.width() + 15 );
			  	
                $popup.find('#large_image').css({ width:'100%', height:'auto' });
                $popup.find('iframe').css({ width:'auto', height:'100%' });
                $popup.find('iframe iframe').css({ maxWidth:'100%', display:'block', clear:'both' });
		  	}
					
		});
		
		return this;
	};

	// default config
	$.bookshelfSlider.config = {
		'item_width': 355,
		'item_height': 320,
		'products_box_margin_left': 20,
		'product_title_textcolor': '#ffffff',
		'product_title_bgcolor': '#c33b4e',
		'product_margin': 30,
		'product_show_title': true,
		'show_title_in_popup': true,
		'show_selected_title': true,
		'show_icons': true,
		'buttons_margin': 10,
		'buttons_align': 'center',
		'slide_duration': 1000,
		'slide_easing': 'easeInOutExpo',
		'arrow_duration': 800,
		'arrow_easing': 'easeInOutExpo',
		'video_width_height': [500,290],
		'iframe_width_height': [400,300],
		'thumbs_effects': true,
		'shelf_space':'small',
		'css_class':'sk01',
		'forced_centralized': false
	};
	
})(jQuery);



/*
///////////////////////////////////////////////////////////
(function($) {
	$(window).load(function() {
		// inserts '.panel_bar' after/before .panel_title
		// use after or before
		$('.panel_title').after($('.panel_bar'));
		
		// optionally hides the 'panel_title'
		$('.panel_title').css({display:'none'});
	});
})(jQuery);
///////////////////////////////////////////////////////////
*/


///////////////////////////////////////////////////////////////
//load other posts on lightbox via ajax with click in an link 

//How to use. insert the link below in bookshelf lightbox
//<a class="open-link" href="javascript:;" data-post-type="bookshelf" data-post-id="1790">test link</a>

(function($) {
    $(window).load(function() {
        //$(selector).delegate(childSelector,event,data,function)
        $(document).delegate('a.open-link', 'click', function() { 
            
            $( ".bs_html_content_lightbox" ).html( 'loading... ' );

            var data = {
                action: 'bs_query_ajax',
                post_id: $(this).data('post-id'),
                post_posttype: $(this).data('post-type')
            };
            
            $.get(bookshelf_script.ajaxurl, data, function(data) {				
                $( ".bs_html_content_lightbox" ).html( data );
            });
        });
    });
})(jQuery);

//-----
/*
(function($) {
    $(window).load(function() {

        $('.bs_product').on('click', function() {
            var bookID = $(this).data('id');
            var intervalChangeColor = setInterval( function() {
                changeBgColor(bookID);
            }, 1000);

            function changeBgColor(bookID) {
                if ( $( "#popup_info .bs_html_content_lightbox" ).length > 0 ) {

                    if ( bookID == '1806' ) {
                        $( "#popup_info .bs_html_content_lightbox" ).css({ background: '#f90' });
                    } else if ( bookID == '352' ) {
                        $( "#popup_info .bs_html_content_lightbox" ).css({ background: '#29aa8c' });
                    }

                    clearInterval(intervalChangeColor);
                }
            }
        }); 

    });
})(jQuery);
*/
///////////////////////////////////////////////////////////////



//-----------------------------------------------------------------
// * jQuery Easing v1.3 - http://gsgd.co.uk/sandbox/jquery/easing/
//-----------------------------------------------------------------

jQuery.easing["jswing"]=jQuery.easing["swing"];jQuery.extend(jQuery.easing,{def:"easeOutQuad",swing:function(e,t,n,r,i){return jQuery.easing[jQuery.easing.def](e,t,n,r,i)},easeInQuad:function(e,t,n,r,i){return r*(t/=i)*t+n},easeOutQuad:function(e,t,n,r,i){return-r*(t/=i)*(t-2)+n},easeInOutQuad:function(e,t,n,r,i){if((t/=i/2)<1)return r/2*t*t+n;return-r/2*(--t*(t-2)-1)+n},easeInCubic:function(e,t,n,r,i){return r*(t/=i)*t*t+n},easeOutCubic:function(e,t,n,r,i){return r*((t=t/i-1)*t*t+1)+n},easeInOutCubic:function(e,t,n,r,i){if((t/=i/2)<1)return r/2*t*t*t+n;return r/2*((t-=2)*t*t+2)+n},easeInQuart:function(e,t,n,r,i){return r*(t/=i)*t*t*t+n},easeOutQuart:function(e,t,n,r,i){return-r*((t=t/i-1)*t*t*t-1)+n},easeInOutQuart:function(e,t,n,r,i){if((t/=i/2)<1)return r/2*t*t*t*t+n;return-r/2*((t-=2)*t*t*t-2)+n},easeInQuint:function(e,t,n,r,i){return r*(t/=i)*t*t*t*t+n},easeOutQuint:function(e,t,n,r,i){return r*((t=t/i-1)*t*t*t*t+1)+n},easeInOutQuint:function(e,t,n,r,i){if((t/=i/2)<1)return r/2*t*t*t*t*t+n;return r/2*((t-=2)*t*t*t*t+2)+n},easeInSine:function(e,t,n,r,i){return-r*Math.cos(t/i*(Math.PI/2))+r+n},easeOutSine:function(e,t,n,r,i){return r*Math.sin(t/i*(Math.PI/2))+n},easeInOutSine:function(e,t,n,r,i){return-r/2*(Math.cos(Math.PI*t/i)-1)+n},easeInExpo:function(e,t,n,r,i){return t==0?n:r*Math.pow(2,10*(t/i-1))+n},easeOutExpo:function(e,t,n,r,i){return t==i?n+r:r*(-Math.pow(2,-10*t/i)+1)+n},easeInOutExpo:function(e,t,n,r,i){if(t==0)return n;if(t==i)return n+r;if((t/=i/2)<1)return r/2*Math.pow(2,10*(t-1))+n;return r/2*(-Math.pow(2,-10*--t)+2)+n},easeInCirc:function(e,t,n,r,i){return-r*(Math.sqrt(1-(t/=i)*t)-1)+n},easeOutCirc:function(e,t,n,r,i){return r*Math.sqrt(1-(t=t/i-1)*t)+n},easeInOutCirc:function(e,t,n,r,i){if((t/=i/2)<1)return-r/2*(Math.sqrt(1-t*t)-1)+n;return r/2*(Math.sqrt(1-(t-=2)*t)+1)+n},easeInElastic:function(e,t,n,r,i){var s=1.70158;var o=0;var u=r;if(t==0)return n;if((t/=i)==1)return n+r;if(!o)o=i*.3;if(u<Math.abs(r)){u=r;var s=o/4}else var s=o/(2*Math.PI)*Math.asin(r/u);return-(u*Math.pow(2,10*(t-=1))*Math.sin((t*i-s)*2*Math.PI/o))+n},easeOutElastic:function(e,t,n,r,i){var s=1.70158;var o=0;var u=r;if(t==0)return n;if((t/=i)==1)return n+r;if(!o)o=i*.3;if(u<Math.abs(r)){u=r;var s=o/4}else var s=o/(2*Math.PI)*Math.asin(r/u);return u*Math.pow(2,-10*t)*Math.sin((t*i-s)*2*Math.PI/o)+r+n},easeInOutElastic:function(e,t,n,r,i){var s=1.70158;var o=0;var u=r;if(t==0)return n;if((t/=i/2)==2)return n+r;if(!o)o=i*.3*1.5;if(u<Math.abs(r)){u=r;var s=o/4}else var s=o/(2*Math.PI)*Math.asin(r/u);if(t<1)return-.5*u*Math.pow(2,10*(t-=1))*Math.sin((t*i-s)*2*Math.PI/o)+n;return u*Math.pow(2,-10*(t-=1))*Math.sin((t*i-s)*2*Math.PI/o)*.5+r+n},easeInBack:function(e,t,n,r,i,s){if(s==undefined)s=1.70158;return r*(t/=i)*t*((s+1)*t-s)+n},easeOutBack:function(e,t,n,r,i,s){if(s==undefined)s=1.70158;return r*((t=t/i-1)*t*((s+1)*t+s)+1)+n},easeInOutBack:function(e,t,n,r,i,s){if(s==undefined)s=1.70158;if((t/=i/2)<1)return r/2*t*t*(((s*=1.525)+1)*t-s)+n;return r/2*((t-=2)*t*(((s*=1.525)+1)*t+s)+2)+n},easeInBounce:function(e,t,n,r,i){return r-jQuery.easing.easeOutBounce(e,i-t,0,r,i)+n},easeOutBounce:function(e,t,n,r,i){if((t/=i)<1/2.75){return r*7.5625*t*t+n}else if(t<2/2.75){return r*(7.5625*(t-=1.5/2.75)*t+.75)+n}else if(t<2.5/2.75){return r*(7.5625*(t-=2.25/2.75)*t+.9375)+n}else{return r*(7.5625*(t-=2.625/2.75)*t+.984375)+n}},easeInOutBounce:function(e,t,n,r,i){if(t<i/2)return jQuery.easing.easeInBounce(e,t*2,0,r,i)*.5+n;return jQuery.easing.easeOutBounce(e,t*2-i,0,r,i)*.5+r*.5+n}})