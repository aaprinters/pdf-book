<?php
/*
Plugin Name:Manage variable products
Plugin URI: http://www.smartlifestore.com/contact_us
Description:This plug-in is used to Manage variable products submited by the users.
Author: WpRight
Version: 1.0
*/

if(! class_exists('Wprt_books')){
	class Wprt_books {
		function Wprt_books(){
		add_action( 'wp_enqueue_scripts', array( $this,'wprt_scripts') );
		add_shortcode('show_books_uploader' , array( $this, 'books_uploader' ));
		
		
		}

	function wprt_scripts() {
			wp_enqueue_style( 'rt_bootstrap1',  plugins_url().'/khana-site/css/bootstrap.css' );
			wp_enqueue_style( 'rt_bootstrap',  plugins_url().'/khana-site/css/bootstrap-multiselect.css' );
			wp_enqueue_script( 'rt_bootstrap', plugins_url().'/khana-site/js/bootstrap-multiselect.js');
	
		}		
		
        function create_book_tax() {
			register_taxonomy(
				'dishes',
				'product',
				array(
					'label' => __( 'Dishes' ),
					'rewrite' => array( 'slug' => 'dishes' ),
					'hierarchical' => true,
				)
			);
		}
		function books_uploader(){
			if ( ! function_exists( 'wp_handle_upload' ) ){
			require_once( ABSPATH . 'wp-admin/includes/file.php' );
			}
			global $post;
			if(isset($_POST['submit_dish'])){
				$rt_name= $_POST['rt_name'];
				$rt_desc= $_POST['rt_desc'];
				$rt_ingredients= $_POST['rt_ingredients'];
				$tags= $_POST['rt_tags'];
				$rt_price= $_POST['rt_price'];
				$rt_phone= $_POST['rt_phone'];
				$uploadedfile = $_FILES['rt_media'];
				$upload_overrides = array( 'test_form' => false );
				$movefile = wp_handle_upload( $uploadedfile, $upload_overrides );
				$my_post = array(
				  'post_title'    		 => $rt_name,
				  'post_content' 		 => $rt_desc,
				  'post_status'          => 'publish',				  
				  'post_type'            => 'product'
				  );
				// Insert the post into the database
				$post_id = wp_insert_post( $my_post );
				$user_id = get_current_user_id();
				$user_data= get_userdata($user_id);
				$user_name = $user_data->user_login;
				wp_set_object_terms( $post_id, $tags, 'product_tag');
				$category_id = wp_set_object_terms( $post_id,$user_name, 'product_cat');
				//$dish_id = wp_set_object_terms( $post_id, $rt_name, 'dishes');
				$cat_id=$category_id[0];
				//$dish_arr_id=$dish_id[0];
				wp_set_post_terms( $post_id, $cat_id, 'product_cat');
				$category_link = get_term_link( $cat_id, 'product_cat' );
				update_post_meta( $post_id,'_price', $rt_price );
				update_post_meta( $post_id,'_regular_price', $rt_price );
				update_post_meta( $post_id, 'ingredients', $rt_ingredients );
				update_post_meta( $post_id, '_visibility', 'visible' );
				global $wpdb;
				$table_name =  'wp_term_relationships';
				$results = $wpdb->get_results("SELECT  `term_id` FROM  `wp_term_taxonomy` WHERE  `taxonomy` =  'product_type'LIMIT 0 , 1");
				//$taxonomy_id=$results->term_id;
				$taxonomyobj=$results[0];
				//print_r($taxonomyobj);
				$taxonomy_id=$taxonomyobj->term_id;
				$user_id = get_current_user_id();
				$user_data= get_userdata($user_id);
				$user_name = $user_data->user_login;
				$store=get_page_by_title($user_name,OBJECT,'wpsl_stores');
				$store_id=$store->ID;
				//print_r($category_link);
				update_post_meta( $store_id, 'wpsl_url', $category_link );
				$wpdb->insert(
                   $table_name,
                   array(
                       'object_id' => $post_id,
                       'term_taxonomy_id' => $taxonomy_id
                   ),
                   array(
                       '%d',
                       '%d'
                   )
				);
				 $wpdb->insert(
                   $table_name,
                   array(
                       'object_id' => $post_id,
                       'term_taxonomy_id' => $cat_id
                   ),
                   array(
                       '%d',
                       '%d'
                   )
				);
				$attachment = array(
				'guid' => $movefile['url'] . '/' . basename( $movefile['file'] ), 
				'post_mime_type' => $movefile['type'],
				'post_title' => preg_replace( '/\.[^.]+$/', '', basename( $movefile['file'] ) ),
				'post_content' => '',
				'post_status' => 'publish',
				'key' => '_visibility'
				);
				
				$attach_id = wp_insert_attachment( $attachment, $movefile['file'], $post_id );
				if($attach_id){
				set_post_thumbnail( $post_id, $attach_id );
				}
				
				
				}
				if ( is_user_logged_in() ) {
				$user_id = get_current_user_id();
				$user_data= get_userdata($user_id);
				
				?>
		
				<div class="container">		
					<div class="row">
						<div class="col-md-3">
						</div>
						<div class="col-md-5">
							<h2>Add Menu </h2><br/>
							<form role="form" action="" method="post" id="addmenu" enctype="multipart/form-data" method="post">
								<div class="form-group">
								<div class="alert alert-success">
								  Loged In As!<strong><?php echo $user_data->display_name; ?></strong> 
								</div>								
								</div>
								<div class="form-group">
									<label class="control-label" for="address">Dish Name:</label>
									<input   placeholder="Enter Dish Name here" style="background-color:white;" type="text" name="rt_name" class="form-control" id="name" required>
								</div>
								<div class="form-group">
									<label class="control-label" for="dish">Description:</label>
									<textarea  placeholder="Enter Description of your Dish here" col="10" rows="8" style="background-color:white;" type="text" name="rt_desc" class="form-control" id="dish" required></textarea>
								</div>
								<div class="form-group">
									<label class="control-label" for="media">Images/Dish Media:</label><br/>
									<input  type="file" name="rt_media" class="btn btn-info btn-block" id="media" multiple >
				
					
								
								</div>
								<div class="form-group">
								<label class="control-label" for="address">Keywords:</label><br/>
								<select id="menu_tags" class="span5 form-control btn-info" multiple="multiple" name="rt_tags[]">
									<option style="color:black;" value="pizza">pizza</option>
									<option style="color:black;" value="burger">burger</option>
									<option style="color:black;" value="chicken">chicken</option>
									<option style="color:black;" value="sea_food">sea food</option>
									<option style="color:black;" value="italian">Italian</option>
								</select>
								<style>
						button.multiselect.dropdown-toggle.btn.btn-default {
							width: 210px !important;
							background-color: white;
						}
						</style>
				<script type="text/javascript" src="http://khana.smartlifestore.com/wp-content/plugins/khana-site/js/bootstrap.min.js"></script> 

				<script type="text/javascript" src="http://khana.smartlifestore.com/wp-content/plugins/khana-site/js/jquery.min.js"></script>
		
				<script src="http://khana.smartlifestore.com/wp-content/plugins/khana-site/js/bootstrap-multiselect.js"
					type="text/javascript"></script> 
								<script type="text/javascript">
									$(function () {
										$('#menu_tags').multiselect({
											includeSelectAllOption: true
										});
									});
									
									$('#addmenu').validate({
									rules: {
										name: {
											minlength: 10,
											maxlength: 15,
											required: true
										},
										rt_desc: {
											minlength: 30,
											maxlength: 35,
											required: true
										}
									},
									highlight: function(element) {
										$(element).closest('.form-group').addClass('has-error');
									},
									unhighlight: function(element) {
										$(element).closest('.form-group').removeClass('has-error');
									},
									errorElement: 'span',
									errorClass: 'help-block',
									errorPlacement: function(error, element) {
										if(element.parent('.input-group').length) {
											error.insertAfter(element.parent());
										} else {
											error.insertAfter(element);
										}
									}
								});
		
								</script>
									<p class="control-label" for="tags">Please select multiple Keywords.</p>
								</div>
								
								<div class="form-group">
									<label class="control-label" for="address">Ingredients:</label>
									<textarea  placeholder="Enter Ingredients of Dish here" col="10" rows="8" style="background-color:white;" type="text" name="rt_ingredients" class="form-control" id="dish" required></textarea>									
								</div>
								<div class="form-group">
									<label class="control-label" for="address">Price:</label>
									<input placeholder="Enter Price here" style="background-color:white;" type="text" name="rt_price" class="form-control" id="name" required>									
								</div>
								
								<br/><br/>
								<input type="submit" name="submit_dish" value="Submit Dish" class="btn btn-primary btn-block">
							</form>
						</div>
						
					</div>
				</div>
				<?php
				}
				else{
				$site_url=wp_login_url ( );
				
				?>
				<div> Please <a href="<?php echo $site_url; ?>">login</a> to add your Menu. </div>
				<?php
				}
		}
	}
}
new Wprt_books();
